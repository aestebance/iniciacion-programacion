var list = [];

do {
    var num;
    strIn = prompt("Introduce un número o \"exit\" para salir:");

    if (strIn == "exit") {                                          //comprobamos que no sea la orden de salir
        break;
    }

    if (!isNaN(num = Number.parseInt(strIn))) {                     //pasamos el string a int y comprobamos si la entrada era un número
        if (list.length == 0) {                                     // si la lista está vacía lo metemos directamente
            list.push(num);                                         // Agregamos el primer número al array
        } else {
            for (var i = 0; i<list.length; i++) {                 //recorremos la lista
                if (num < list[i]) {                             // comprobamos si número es menor que el número en listado.
                    list.splice(i, 0, num);                     // introducimos el número y empujamos el resto de números
                    break;
                } else if (i == list.length - 1) {              //Si es el último valor de la lista lo metemos al final antes de salir del bucle
                    list.push(num);    
                    break;                         //introducimos el número al final del array
                }
            }
        }
        console.log(list);
    }

} while(true);
console.log(list);